<?php

/**
 * @file
 * Hooks provided by the Apache Solr Statistics module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defined a query log backend.
 *
 * @return array
 *   An associative array keyed by the machine name of the backend to an
 *   associative array containing:
 *   - label: The human readable name of the backend.
 *   - class: The class containing the backend adapter.
 */
function hook_apachesolr_stats_backends() {
  $adapters = array();

  $adapters['database'] = array(
    'label' => t('Database'),
    'class' => 'Drupal\ApachesolrStats\Backend\DatabaseBackend'
  );

  return $adapters;
}

/**
 * Define criteria that bypass query logging.
 *
 * @return array
 *   An assocaitive array keyed by machine name of the bypass criterion to an
 *   asociative array containing:
 *   - label: The human readable name of the criterion that serves as the title
 *     of the vertical tab containing its settings in the admin UI.
 *   - ignore callback: The function that returns a boolean flagging whether to
 *     bypass the query log.
 *   - settings callback: A function that adds the form elements to the admin
 *     UI for this criterion.
 */
function hook_apachesolr_stats_bypass() {
  $bypass = array();

  $bypass['ip'] = array(
    'label' => t('Ignore IP adresses'),
    'ignore callback' => 'apachesolr_stats_ignore_ip',
    'settings callback' => 'apachesolr_stats_ignore_ip_settings',
  );

  return $bypass;
}

/**
 * Captures statistics data and adds to the data array passed to the backend.
 *
 * @param DrupalSolrQueryInterface $query
 *   The query object containing the executed query.
 * @param stdClass $response
 *   The response object for the executed query as returned by
 *   apachesolr_static_response_cache().
 * @param array $context
 *   An array of data returned by apachesolr_stats_get_query_context()
 *   containing contextual information about the executed search.
 *
 * @return array
 *   An associative array keyed by field name to the data baing collected for
 *   the field.
 */
function hook_apachesolr_stats_data(DrupalSolrQueryInterface $query, stdClass $response, array $context) {
  return array(
    'field1' => 'somevalue',
    'field2' => array('more', 'values'),
  );
}

/**
 * Defines the report plugin classes and which backends they work with.
 *
 * @return array
 *   An associative array keyed by machine name of the report an associative
 *   array containing:
 *   - label: The human readable name of the report.
 *   - report plugin: An associative array keyed by the machine name of the
 *     backend to the class the generates the raw report data.
 *   - visualization plugin: An associative array keyed by the machine name of
 *     the backend to the class the generates the raw report data.
 */
function hook_apachesolr_stats_reports() {
  $reports = array();

  $reports['user'] = array(
    'label' => t('User data'),
    'report plugin' => array(
      'database' => 'Drupal\ApachesolrStats\Backend\Database\Report\User',
    ),
    'visualization plugin' => array(
      'google' => 'Drupal\ApachesolrStats\Visualization\Google\GooglePieChart',
    ),
  );

  return $reports;
}

/**
 * Defines the report plugin classes and which backends they work with.
 *
 * @param array &$reports
 *   The report definitions returned by hook_apachesolr_stats_reports()
 *   implementations.
 */
function hook_apachesolr_stats_reports_alter(&$reports) {
  $reports['top_keywords']['report plugin']['mybackend'] = 'MyProject\MyClass';
}

/**
 * @} End of "addtogroup hooks".
 */
