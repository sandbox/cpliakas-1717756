<?php

/**
 * @file
 * High performance search statistics callback.
 */

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());

// Only bootstrap the database.
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

try {
  // Load the required information from the database, key by the "name" column.
  $variables = db_query('SELECT name, value FROM {apachesolr_stats}')->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
  if (empty($variables['module_dir'][0])) {
    throw new Exception('The "module_dir" variable is required.');
  }
}
catch (Exception $e) {
  // @todo Can we log this in a generic way?
  header('HTTP/1.1 503 Service Unavailable', TRUE, 503);
}

// Include the base functions, run the event logger application.
require_once $variables['module_dir'][0] . '/apachesolr_stats.common.inc';
$event_logger = new Drupal\ApachesolrStats\EventLogger($variables);
$event_logger->run();
