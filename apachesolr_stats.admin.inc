<?php

/**
 * @file
 * Administrative forms for the Apache Solr Statistics module.
 */

/**
 * Administrative settings form.
 *
 * @ingroup forms
 */
function apachesolr_stats_admin($form, &$form_state) {

  $form['apachesolr_stats:query_log_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable query log'),
    '#default_value' => variable_get('apachesolr_stats:query_log_enabled', 0),
    '#description' => t('Capture statistics in the query log about searches being executed.'),
  );

  $form['apachesolr_stats:event_log_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable event log'),
    '#default_value' => variable_get('apachesolr_stats:event_log_enabled', 0),
    '#description' => t('Capture statistics in the event og about actions taken on the content returned in search results.'),
  );

  $form['data_settings_title'] = array(
    '#type' => 'item',
    '#title' => t('Data capture settings'),
  );

  $form['apachesolr_stats:capture_user_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capture user data'),
    '#default_value' => variable_get('apachesolr_stats:capture_user_data', 0),
    '#description' => t('Log information about the user who executed the search.'),
  );

  $form['apachesolr_stats:capture_performance_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capture search performance data'),
    '#default_value' => variable_get('apachesolr_stats:capture_performance_data', 0),
    '#description' => t('Cature performance information, for example the time it takes for Solr to prepare and execute the query. This setting requires passing the debugParam to Solr, which consumes a significant amount of resources.'),
  );

  $form['bypass_settings_title'] = array(
    '#type' => 'item',
    '#title' => t('Log bypass settings'),
  );

  $form['bypass_settings'] = array(
    '#type' => 'vertical_tabs',
  );

  $bypass_info = module_invoke_all('apachesolr_stats_bypass');
  foreach ($bypass_info as $name => $info) {
    if (!empty($info['settings callback'])) {
      $element_name = 'bypass_' . $name;

      $form[$element_name] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($info['label']),
        '#group' => 'bypass_settings'
      );

      $form[$element_name] += call_user_func($info['settings callback']);
    }
  }

  $intervals = drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
  $form['apachesolr_stats:purge_interval'] = array(
    '#type' => 'select',
    '#title' => t('Log retention policy'),
    '#default_value' => variable_get('apachesolr_stats:purge_interval', 259200),
    '#options' => $intervals,
    '#description' => t('Automatically purge log entries older than this threshold.'),
  );

  $form['apachesolr_stats:backend'] = array(
    '#type' => 'select',
    '#title' => t('Log backend'),
    '#options' => apachesolr_stats_get_backend_options(),
    '#default_value' => variable_get('apachesolr_stats:backend', 'database'),
    '#description' => t('Select the query log backend.'),
  );

  $form['#submit'][] = 'apachesolr_stats_admin_submit';
  return system_settings_form($form);
}

/**
 * Returns an array of backend options.
 *
 * @return array
 *   An associative array keyed by machine name of the backend to the sanitized
 *   human readable name of the backend. The array can be used directly as
 *   option in FAPI elements.
 */
function apachesolr_stats_get_backend_options() {
  $options = array();
  $backend_info = module_invoke_all('apachesolr_stats_backends');
  foreach ($backend_info as $name => $info) {
    $options[$name] = check_plain($info['label']);
  }
  return $options;
}

/**
 * Form submission handler for apachesolr_stats_admin().
 *
 * The high performance callback doesn't bootstrap the variable system, so we
 * also store the adapter class in a separate table. We could query the table
 * directly, but then we would have to resolve the class anyways. This is just
 * way easier. Well, for me anyways.
 */
function apachesolr_stats_admin_submit($form, &$form_state) {
  apachesolr_stats_rebuild_variables_table();
}

/**
 * Rebuilds the apachesolr_stats table.
 */
function apachesolr_stats_rebuild_variables_table() {
  $txn = db_transaction();

  db_delete('apachesolr_stats')->execute();

  db_insert('apachesolr_stats')
  ->fields(array(
    'name' => 'module_dir',
    'value' => drupal_get_path('module', 'apachesolr_stats'),
  ))
  ->execute();

  db_insert('apachesolr_stats')
  ->fields(array(
    'name' => 'backend_class',
    'value' => apachesolr_stats_get_backend_class(),
  ))
  ->execute();
  
  db_insert('apachesolr_stats')
  ->fields(array(
    'name' => 'key',
    'value' => apachesolr_stats_get_key(),
  ))
  ->execute();
}
