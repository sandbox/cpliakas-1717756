<?php

/**
 * @file
 * Apache Solr Statistics hook implementations.
 */

/**
 * Implements hook_apachesolr_stats_backends().
 */
function apachesolr_stats_apachesolr_stats_backends() {
  $adapters = array();

  $adapters['database'] = array(
    'label' => t('Database'),
    'class' => 'Drupal\ApachesolrStats\Backend\DatabaseBackend'
  );

  return $adapters;
}

/**
 * Implements hook_apachesolr_stats_bypass().
 */
function apachesolr_stats_apachesolr_stats_bypass() {
  $bypass = array();

  $bypass['role'] = array(
    'label' => t('Roles'),
    'bypass callback' => 'apachesolr_stats_bypass_role',
    'settings callback' => 'apachesolr_stats_bypass_role_settings',
  );

  $bypass['ip'] = array(
    'label' => t('IP adresses'),
    'bypass callback' => 'apachesolr_stats_bypass_ip',
    'settings callback' => 'apachesolr_stats_bypass_ip_settings',
  );

  return $bypass;
}

/**
 * Checks the requesting IP address against the blacklist.
 *
 * @param stdClass $account
 *   The user object roles are being checked for.
 *
 * @return boolean
 *   A boolean flagging whether user's role is blocked.
 */
function apachesolr_stats_bypass_role($account = NULL) {
  if (NULL === $account) {
    global $user;
    $account = $user;
  }
  $bypassed_roles = variable_get('apachesolr_stats:bypassed_roles', array());
  $test = array_intersect_key($account->roles, array_filter($bypassed_roles));
  return !empty($test);
}

/**
 * Settings callback for the "role" bypass criterion.
 *
 * @return array
 *   A FAPI array containing the settings.
 */
function apachesolr_stats_bypass_role_settings() {

  $form['apachesolr_stats:bypassed_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Do not capture statistics for users of the selected role(s)'),
    '#options' => user_roles(),
    '#default_value' => variable_get('apachesolr_stats:bypassed_roles', array()),
    '#description' => t('Select the roles who will bypass query logging.'),
  );

  return $form;
}

/**
 * Checks the requesting IP address against the blacklist.
 *
 * @param string $requesting_ip
 *   The requesting IP address.
 *
 * @return boolean
 *   A boolean flagging whether logging should be bypassed based on IP.
 */
function apachesolr_stats_bypass_ip($requesting_ip = NULL) {
  $bypassed_ips = variable_get('apachesolr_stats:bypassed_ips', '');
  if ($bypassed_ips) {
    if (NULL === $requesting_ip) {
      $requesting_ip = ip_address();
    }
    $bypassed_ips = preg_split('/[\s]+/', $bypassed_ips);
    foreach ($bypassed_ips as $bypassed_ip) {
      if ($bypassed_ip && strpos($requesting_ip, $bypassed_ip) === 0) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Settings callback for the "ip" bypass criterion.
 *
 * @return array
 *   A FAPI array containing the settings.
 */
function apachesolr_stats_bypass_ip_settings() {

  $form['apachesolr_stats:bypassed_ips'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not capture statistics for the IP address(es) below'),
    '#default_value' => variable_get('apachesolr_stats:bypassed_ips', ''),
    '#description' => t('Enter IP addresses (e.g.: 192.168.1.2), one per line. You can match entire subnets using a partial IP address ending with a period (e.g.: 192.168.)'),
  );

  return $form;
}

/**
 * Implements hook_apachesolr_stats_data().
 */
function apachesolr_stats_apachesolr_stats_data(DrupalSolrQueryInterface $query, stdClass $response, array $context) {

  $data = array(
    'query_id' => $context['query_id'],
    'timestamp' => REQUEST_TIME,
    'site_hash' => apachesolr_site_hash(),
    'env_id' => $query->solr('getId'),
    'page_id' => apachesolr_stats_get_page_id(),
    'keywords' => drupal_strtolower($query->getParam('q')),
    'facets' => apachesolr_stats_get_active_facets($query->getSearcher()),
    'matched_documents' => apachesolr_stats_get_matched_docs($response),
    'num_found' => $response->response->numFound,
    'num_suggestions' => !empty($response->spellcheck->suggestions) ? 1 : 0,
    'page' => pager_find_page(),
    'rows' => $query->getParam('rows'),
    'sort' => $query->getSolrsort(),
  );

  // Capture user data if configured to do so.
  if (variable_get('apachesolr_stats:capture_user_data', 0)) {
    global $user;
    $data['uid'] = $user->uid;

    // It only makes sense to capture the session ID for authenticated users
    // since the ID changes for anonymous users on every page load.
    if ($user->uid) {
      $data['session_id'] = session_id();
    }
  }

  // Capture performance data if configured to do so.
  if (variable_get('apachesolr_stats:capture_performance_data', 0) && isset($response->debug->timing)) {
    $data += array(
      'total_time' => $response->debug->timing->time,
      'prepare_time' => $response->debug->timing->prepare->time,
      'process_time' => $response->debug->timing->process->time,
    );
  }

  return $data;
}

/**
 * Calculates ID of the search page.
 *
 * @return string|NULL
 *   The page ID of the search page, NULL if it could not be calculated.
 */
function apachesolr_stats_get_page_id() {
  $page_id = NULL;

  // Hacky method that attempts to calculate the search page ID.
  $pages = &drupal_static('apachesolr_search_page_load', array());
  foreach ($pages as $cur_page_id => $page) {
    $path = str_replace('%', '*', $page->search_path);
    $pattern = "$path\n$path/*";
    if (drupal_match_path(current_path(), $pattern)) {
      $page_id = $cur_page_id;
      break;
    }
  }

  return $page_id;
}

/**
 * Gets the matched documents.
 *
 * @param stdClass $response
 *   The response object for the executed query as returned by
 *   apachesolr_static_response_cache().
 *
 *
 * @return array
 *   An array of unique document IDs as created by apachesolr_document_id().
 */
function apachesolr_stats_get_matched_docs(stdClass $response) {
  $docs = array();
  if (!empty($response->response->docs)) {
    foreach ($response->response->docs as $doc) {
      $docs[] = $doc->id;
    }
  }
  return $docs;
}

/**
 * Gets the searcher's active facets from Facet API.
 *
 * @param string $searcher
 *   The machine name of the searcher.
 *
 * @return array
 *   An associative array keyed by facet name to active value.
 */
function apachesolr_stats_get_active_facets($searcher) {
  $filters = array();
  if (function_exists('facetapi_adapter_load')) {
    if ($adapter = facetapi_adapter_load($searcher)) {
      $active = $adapter->getAllActiveItems();
      foreach ($active as $filter) {
        foreach ($filter['facets'] as $facet_name) {
          $filters[$facet_name] = $filter['value'];
        }
      }
    }
  }
  return $filters;
}

/**
 * Implements hook_apachesolr_stats_reports().
 */
function apachesolr_stats_apachesolr_stats_reports() {
  $reports = array();

  $reports['top_keywords'] = array(
    'label' => t('Top keywords'),
    'description' => t('Displays the most common keywords submitted by end users.'),
    'report plugin' => array(
      'database' => 'Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport',
    ),
  );
  apachesolr_stats_add_standard_plugins($reports['top_keywords']);
  apachesolr_stats_add_google_plugin($reports['top_keywords'], 'GooglePieChart');

  $reports['top_keywords_no_results'] = array(
    'label' => t('Top keywords with no results'),
    'description' => t('Displays the most common keywords submitted by end users that didn\'t produce any results.'),
    'report plugin' => array(
      'database' => 'Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsNoResultsReport',
    ),
  );
  apachesolr_stats_add_standard_plugins($reports['top_keywords_no_results']);
  apachesolr_stats_add_google_plugin($reports['top_keywords_no_results'], 'GooglePieChart');

  $reports['top_keywords_no_click_throughs'] = array(
    'label' => t('Top keywords with no click-throughs'),
    'description' => t('Displays the most common keywords submitted by end users that didn\'t produce any click-throughs.'),
    'report plugin' => array(
      'database' => 'Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsNoClickThroughsReport',
    ),
  );
  apachesolr_stats_add_standard_plugins($reports['top_keywords_no_click_throughs']);
  apachesolr_stats_add_google_plugin($reports['top_keywords_no_click_throughs'], 'GooglePieChart');

  $reports['keyword_click_throughs'] = array(
    'label' => t('Top click-throughs for a keyword.'),
    'description' => t('Displays the most common click-throughs for the given keyword.'),
    'report plugin' => array(
      'database' => 'Drupal\ApachesolrStats\Backend\Database\Report\KeywordClickThroughsReport',
    ),
  );
  apachesolr_stats_add_standard_plugins($reports['keyword_click_throughs']);
  apachesolr_stats_add_google_plugin($reports['keyword_click_throughs'], 'GooglePieChart');

  return $reports;
}

/**
 * Helper function that adds a visualization plugin.
 *
 * @param array $report
 *   The report definition.
 * @param string $class
 *   The name of the class in the Drupal\ApachesolrStats\Visualization\Google
 *   namespace.
 */
function apachesolr_stats_add_google_plugin(&$report, $class) {
  static $chart_module_exists;
  if (NULL === $chart_module_exists) {
    $chart_module_exists = module_exists('chart');
  }
  if ($chart_module_exists) {
    $report['visualization plugin']['google'] = 'Drupal\ApachesolrStats\Visualization\Google\\' . $class;
  }
}

/**
 * Helper function that adds the standard visualization plugins.
 *
 * @param array $report
 *   The report definition.
 */
function apachesolr_stats_add_standard_plugins(&$report) {
  if (empty($report['visualization plugin'])) {
    $report['visualization plugin'] = array();
  }
  $report['visualization plugin'] += array(
    'json' => 'Drupal\ApachesolrStats\Visualization\JsonVisualization',
    'csv' => 'Drupal\ApachesolrStats\Visualization\CsvVisualization',
    'table' => 'Drupal\ApachesolrStats\Visualization\TableVisualization',
  );
}
