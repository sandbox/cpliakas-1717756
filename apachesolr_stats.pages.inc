<?php

/**
 * @file
 * Page callbacks that show the search report statistics.
 */

use Drupal\ApachesolrStats\Visualization\Google\GooglePieChart as GooglePieChart;

/**
 * Callback for admin/reports/apachesolr/%apachesolr_environment/stats.
 */
function apachesolr_stats_report($environment) {
  $build = array();

  $table_reports = array(
    'top_keywords',
    'top_keywords_no_results',
    'top_keywords_no_click_throughs',
  );

  foreach ($table_reports as $report_name) {
    $build[$report_name] = apachesolr_stats_render_report($report_name, 'table');
    $build[$report_name]['#empty'] = t('No keywords match this criteria.');
    $build[$report_name]['#header'] = array(
      t('Keywords'),
      t('Count'),
    );
  }

  // Add links to click-through reports for top keywords.
  $build['top_keywords']['#header'][] = t('Operations');
  foreach ($build['top_keywords']['#rows'] as &$row) {
    $path = 'admin/reports/apachesolr/' . $environment['env_id']  . '/stats/click-throughs/' . $row[0];
    $row[] = l(t('Click-through report'), $path);
  }
  unset($row);

  return $build;
}

/**
 *
 */
function apachesolr_keyword_clickthrough_report($environment, $keywords) {
  $build = array();

  $build['link'] = array(
    '#type' => 'link',
    '#title' => t('Go back to statistics overview page'),
    '#href' => 'admin/reports/apachesolr/' . $environment['env_id'] . '/stats',
  );

  $options['keywords'] = $keywords;
  $build['report'] = apachesolr_stats_render_report('keyword_click_throughs', 'table', $options);
  $build['report']['#empty'] = t('There are no click-throughs for the keywords.');
  $build['report']['#header'] = array(
    t('Document'),
    t('Count'),
  );

  foreach ($build['report']['#rows'] as &$row) {
    $row[0] = l($row[0], $row[0], array('absolute' => TRUE));
  }
  unset($row);

  return $build;
}
