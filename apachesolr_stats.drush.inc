<?php

/**
 * @file
 * Drush commands for the Apache Solr Statistics module.
 */

/**
 * The default format used to render reports.
 */
define('APACHESOLR_STATS_DEFAULT_FORMAT', 'csv');

/**
 * Implements hook_drush_command().
 */
function apachesolr_stats_drush_command() {
  $items = array();

  $items['apachesolr-stats-report'] = array(
    'description' => 'Displays report data.',
    'arguments' => array(
      'report-name' => 'The machine name of the report.',
    ),
    'options' => array(
      'format' => 'Output formay, i.e. "csv", "json". Defaults to "' . APACHESOLR_STATS_DEFAULT_FORMAT . '".',
      'keywords' => 'Filter by keywords for reports that support them.',
      'limit' => 'The maximum number of rows to return, defaults to 10.',
      'start-time' => 'Reports will only include data for logs after the specified date.',
      'end-time' => 'Reports will only include data for logs before the specified date.',
    ),
    'aliases' => array('as-report'),
  );

  $items['apachesolr-stats-list-reports'] = array(
    'description' => 'List all available reports.',
    'aliases' => array('as-list'),
  );

  return $items;
}

/**
 * Renders a report.
 *
 * @param string $name
 *   The machine readable name of the report.
 */
function drush_apachesolr_stats_report($name) {
  // Get the format used to return the report.
  if (!$format = drush_get_option('format')) {
    $format = APACHESOLR_STATS_DEFAULT_FORMAT;
  }

  // Build the report options.
  $options = array();
  $option_names = array('keywords', 'limit', 'start-time', 'end-time');
  foreach ($option_names as $option_name) {
    if (NULL !== ($option_value = drush_get_option($option_name))) {

      // Perform any one-off data alterations.
      switch ($option_name) {
        case 'start-time':
        case 'end-time':
          $option_value = strtotime($option_value);
          break;
      }

      // Normalize the option name by converting dashes to underscores so the
      // option matches the array key name in the report class.
      $option_name = str_replace('-', '_', $option_name);
      $options[$option_name] = $option_value;
    }
  }

  // Render the report in machine readable format.
  $report = apachesolr_stats_render_report($name, $format, $options);
  if (FALSE !== $report) {
    drush_print($report);
  }
  else {
    $message = dt('Error generating report. Check that the report name and format are valid.');
    drush_log($message, 'error');
  }
}

/**
 * :Lists all available reports.
 */
function drush_apachesolr_stats_list_reports() {
  $rows = array();

  // Add the header.
  $rows[] = array(
    'name' => dt('Report name'),
    'description' => dt('Description'),
  );

  // Add each report as a row.
  $report_info = apachesolr_stats_get_report_info();
  foreach ($report_info as $name => $info) {
    $rows[] = array(
      'name' => $name,
      'description' => $info['description'],
    );
  }

  drush_print_table($rows, TRUE);
}
