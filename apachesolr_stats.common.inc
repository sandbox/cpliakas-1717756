<?php

/**
 * @file
 * Contains code that can be used in environments where Drupal is not fully
 * bootstrapped. Registers the autoload implementation for this module's
 * library.
 */

/**
 * This module's directory.
 */
define('APACHESOLR_STATS_DIRECTORY', dirname(__FILE__));

// Register the autoloader outside of hooks so the library is always available.
spl_autoload_register('apachesolr_stats_load_class');

/**
 * Very basic class autoloader for our library.
 *
 * @param string $class
 *   The name of the class being autoloaded.
 */
function apachesolr_stats_load_class($class) {
  if (0 === strpos($class, 'Drupal\\ApachesolrStats\\')) {
    if (!class_exists($class, FALSE) && !interface_exists($class, FALSE)) {
      $basename = str_replace('\\', '/', $class) . '.php';
      include_once APACHESOLR_STATS_DIRECTORY . '/lib/' . $basename;
    }
  }
}

/**
 * Gets the parameters from the query string used to generate the hash.
 *
 * @param array $qstring
 *   The passed query string parameters.
 * @param bool $event_log
 *   A boolean flagging whether to include parameters for event logging,
 *   defaults to FALSE.
 *
 * @return array
 *   An associative array of query string parameters.
 */
function apachesolr_stats_get_hash_params(array $qstring, $event_log = FALSE) {

  $whitelist = array(
    'query_id' => '',
    'timestamp' => '',
    'keywords' => '',
  );

  if ($event_log) {
    $whitelist += array(
      'url' => '',
      'document_id' => '',
    );
  }

  return array_intersect_key($qstring, $whitelist);
}

/**
 * Builds a 16 character hash from the array of data.
 *
 * Sorts the array by key, converts to a string, and hashes with the passed
 * salt. The hash is used to ensure that the query string isn't tampered with.
 *
 * @param array $params
 *   An associative array of query string data being hashed.
 * @param string $key
 *   The shared secret key used for generating the HMAC variant of the message
 *   digest.
 *
 * @return string
 *   The HMAC variant of the message digest.
 */
function apachesolr_stats_hmac(array $params, $key = NULL) {
  if (NULL === $key) {
    $key = apachesolr_stats_get_key();
  }
  ksort($params);
  $string = http_build_query($params);
  return substr(hash_hmac('sha1', $string, $key), 0, 16);
}
