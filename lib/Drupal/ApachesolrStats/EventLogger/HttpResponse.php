<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\EventLogger\Response.
 */

namespace Drupal\ApachesolrStats\EventLogger;

/**
 * Sends various response headers,
 */
class HttpResponse {

  /**
   * Helper function that immediately sends a header and exits.
   *
   * @param string $string
   *   The raw header string.
   * @param int $code
   *   The HTTP error code.
   */
  public function sendHeader($string, $code) {
    header($string, TRUE, $code);
    flush();
    exit();
  }

  /**
   * Sends a 204 no content header.
   */
  public function ok() {
    $this->sendHeader('HTTP/1.1 204 No Content', 204);
  }

  /**
   * Sends a 302 redirect header for the passed URL.
   *
   * @param string $url
   *   The URL that the redirect header is beig set for.
   */
  public function redirect($url) {
    $this->sendHeader('Location: ' . $url, 302);
  }

  /**
   * Sends a 400 bad request header.
   */
  public function badRequest() {
    $this->sendHeader('HTTP/1.1 400 Bad Request', 400);
  }

  /**
   * Sends a 503 service unavailable header.
   */
  public function error() {
    $this->setHeader('HTTP/1.1 503 Service Unavailable', 503);
  }
}
