<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Report\Visualization\JsonVisualization.
 */

namespace Drupal\ApachesolrStats\Visualization;

/**
 * Renders reports in an HTML table.
 */
class TableVisualization extends StatsVisualization {

  /**
   * Implements Drupal\ApachesolrStats\Report\Visualization\VisualizationAdapter::render().
   */
  public function render() {

    $rows = array();
    foreach ($this->report->getReportData() as $key => $value) {
      $rows[] = array(
        check_plain($key),
        check_plain($value),
      );
    }

    return array(
      '#caption' => check_plain($this->report_info['label']),
      '#theme' => 'table',
      '#rows' => $rows,
    );
  }
}
