<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Report\Visualization\JsonVisualization.
 */

namespace Drupal\ApachesolrStats\Visualization;

/**
 * Renders reports in JSON.
 */
class JsonVisualization extends StatsVisualization {

  /**
   * Implements Drupal\ApachesolrStats\Report\Visualization\VisualizationAdapter::render().
   */
  public function render() {
    return drupal_json_encode($this->report->getReportData());
  }
}
