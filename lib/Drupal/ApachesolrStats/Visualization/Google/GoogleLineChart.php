<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Visualization\Google\GoogleLineChart.
 */

namespace Drupal\ApachesolrStats\Visualization\Google;

use Drupal\ApachesolrStats\Visualization\StatsVisualization as StatsVisualization;

/**
 * Base class for report generators.
 */
class GoogleLineChart extends StatsVisualization {

  /**
   * Implements Drupal\ApachesolrStats\Visualization::render().
   */
  public function render() {
//    $chart = array(
//      '#chart_id' => 'apachesolr_stats_' . $this->report_info['name'],
//      '#title' => $this->report_info['label'],
//      '#type' => CHART_TYPE_LINE,
//      '#size' => array(
//        '#width' => 250,
//        '#height' => 100,
//      ),
//    );
//    $chart['#data'] = $this->report->getReportData();
//    $chart['#labels'] = array_keys($chart['#data']);
//    return theme('chart', array('chart' => $chart));
  }
}
