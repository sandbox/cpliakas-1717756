<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Report\Visualization.
 */

namespace Drupal\ApachesolrStats\Visualization;

use Drupal\ApachesolrStats\Report\StatsReport as StatsReport;

/**
 * Base class for report
 */
abstract class StatsVisualization {

  /**
   * The report being visualized.
   *
   * @var Drupal\ApachesolrStats\Report\StatsReport $report
   */
  protected $report;

  /**
   * The report hook definition defined in hook_apachesolr_stats_reports().
   *
   * @var array
   */
  protected $report_info;

  /**
   * Constructs a Drupal\ApachesolrStats\Report\Visualization\VisualizationAdapter object.
   *
   * @param Drupal\ApachesolrStats\Report\StatsReport $report
   *   The report being visualized.
   * @param array $report_info
   *   The report hook definition defined in hook_apachesolr_stats_reports().
   */
  public function __construct(StatsReport $report, array $report_info) {
    $this->report = $report;
    $this->report_info = $report_info;
  }

  /**
   * Returns an instance of the extending class.
   *
   * @param Drupal\ApachesolrStats\Report\StatsReport $report
   *   The report being visualized.
   * @param array $report_info
   *   The report hook definition defined in hook_apachesolr_stats_reports().
   */
  public static function getInstance(StatsReport $report, array $report_info) {
    $called_class = get_called_class();
    return new $called_class($report, $report_info);
  }

  /**
   * Renders the data for display.
   */
  abstract public function render();
}
