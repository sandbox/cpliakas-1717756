<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Report\Visualization\JsonVisualization.
 */

namespace Drupal\ApachesolrStats\Visualization;

/**
 * Renders reports in CSV.
 */
class CsvVisualization extends StatsVisualization {

  /**
   * Implements Drupal\ApachesolrStats\Report\Visualization\VisualizationAdapter::render().
   */
  public function render() {
    $output = '';
    foreach ($this->report->getReportData() as $key => $value) {
      $output .= '"' . $key . '","' . $value . '"' . PHP_EOL;
    }
    return rtrim($output);
  }
}
