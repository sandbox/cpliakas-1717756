<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Report\StatsReport.
 */

namespace Drupal\ApachesolrStats\Report;

use Drupal\ApachesolrStats\Backend\StatsBackend as StatsBackend;

/**
 * Base class for report generators.
 */
abstract class StatsReport {

  const DEFAULT_LIMIT = 10;

  /**
   * The backend adapter that interacts with the statistics logs.
   *
   * @var Drupal\ApachesolrStats\Backend\StatsBackend
   */
  protected $backend;

  /**
   * Report generation options.
   *
   * @var array
   */
  protected $options = array();

  /**
   * Constructs a Drupal\ApachesolrStats\Report\StatsReport object.
   *
   * @param Drupal\ApachesolrStats\Backend\StatsBackend $backend
   *   The backend adapter that interacts with the statistics logs.
   * @param array $options
   *   An associative array of report generation options.
   */
  public function __construct(StatsBackend $backend, array $options = array()) {
    $this->backend = $backend;
    $this->options = $options;
  }

  /**
   * Returns report data in a structured array.
   *
   * @return array
   *   A structured array of report data.
   */
  abstract public function getReportData();
}
