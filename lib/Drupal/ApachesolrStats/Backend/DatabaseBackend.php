<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\DatabaseBackend
 */

namespace Drupal\ApachesolrStats\Backend;

/**
 * Stores statistics in a database.
 */
class DatabaseBackend extends StatsBackend {

  /**
   * Implements Drupal\ApachesolrStats\Backend\BackendAdapter::write().
   */
  public function write(array $data, $bin, array $options = array()) {
    array_walk($data, array($this, 'serialize'));
    db_insert($bin)->fields($data)->execute();
  }

  /**
   * Implements Drupal\ApachesolrStats\Backend\BackendAdapter::purge().
   */
  public function purge($bin, array $options = array()) {
    $timestamp = REQUEST_TIME - variable_get('apachesolr_stats:purge_interval', 259200);
    db_delete($bin)->condition('timestamp', $timestamp, '<')->execute();
  }

  /**
   * Callback for array_walk() that serializes the value if it is an array.
   *
   * @param type &$value
   *   The value of the current item in the array being walked.
   * @param type $key
   *   The key of the current item in the array being walked.
   */
  public function serialize(&$value, $key) {
    if (is_array($value)) {
      $value = serialize($value);
    }
  }

  /**
   * Helper function that executes a database query and returns an array.
   *
   * @param SelectQuery $query
   *   The database query being executed.
   * @param string $key
   *   The name of the column containing the values used as the array keys.
   * @param string $value
   *   The name of the colums containing the values used as the array values.
   *
   * @return array
   *   An array containing the report value.
   */
  public function queryToArray(\SelectQuery $query, $key, $value = 'num') {
    $data = array();
    $result = $query->execute();
    foreach ($result as $record) {
      $data[$record->$key] = $record->$value;
    }
    return $data;
  }
}
