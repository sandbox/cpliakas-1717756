<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport.
 */

namespace Drupal\ApachesolrStats\Backend\Database\Report;

use Drupal\ApachesolrStats\Report\StatsReport as StatsReport;
use Drupal\ApachesolrStats\Backend\StatsBackend as StatsBackend;

/**
 * Base class for reports that act on the top keywords.
 */
class TopKeywordsBase extends StatsReport {

  /**
   * Helper function that returns the inner query for keyword reports.
   *
   * @param array $options
   *   An associative array of report generation options.
   *
   * @return SelectQuery
   *   The inner select query object.
   */
  public function getInnerQuery(array $options) {
    $query = db_select(StatsBackend::QUERY_LOG, 'asql', array('target' => 'slave'))
      ->fields('asql', array('keywords'))
      ->groupBy('asql.query_id')
      ->groupBy('asql.keywords');

    if ($options['start_time']) {
      $query->condition('asql.timestamp', $options['start_time'], '>=');
    }
    if ($options['end_time']) {
      $query->condition('asql.timestamp', $options['end_time'], '<=');
    }

    return $query;
  }

  /**
   * Helper function that returns the outer query for keyword reports.
   *
   * @param SelectQuery $inner
   *   The inner query usually returned by TopKeywordsBase::getInnerQuery().
   * @param array $options
   *   An associative array of report generation options.
   *
   * @return SelectQuery
   *   The inner select query object.
   */
  public function getOuterQuery(\SelectQuery $inner, array $options) {
    // The outer query performs the counting group by.
    $query = db_select($inner, 'asql_inner', array('target' => 'slave'))
      ->fields('asql_inner', array('keywords'))
      ->groupBy('keywords')
      ->orderBy('num', $options['direction'])
      ->orderBy('keywords');

    $query->addExpression('COUNT(keywords)', 'num');
    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }

    return $query;
  }

  /**
   * Returns options with defaults merged in.
   *
   * @return array
   *   An array of options.
   */
  public function getOptions() {
    return $this->options + array(
      'direction' => 'DESC',
      'limit' => StatsReport::DEFAULT_LIMIT,
      'start_time' => 0,
      'end_time' => 0,
    );
  }

  /**
   * Implements Drupal\ApachesolrStats\Report\StatsReport::getReportData().
   *
   * This must still be implemented by the extending class.
   */
  public function getReportData() {
    return array();
  }
}
