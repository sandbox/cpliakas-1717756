<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport.
 */

namespace Drupal\ApachesolrStats\Backend\Database\Report;

/**
 * Returns report data for keywords.
 */
class TopKeywordsNoResultsReport extends TopKeywordsBase {

  /**
   * Implements Drupal\ApachesolrStats\Report\StatsReport::getReportData().
   */
  public function getReportData() {
    $options = $this->getOptions();

    // Get inner query, add condition to get keywords producing no results.
    $inner_query = $this->getInnerQuery($options);
    $inner_query->condition('num_found', '0');

    // Build the final query, render report.
    $query = $this->getOuterQuery($inner_query, $options);
    return $this->backend->queryToArray($query, 'keywords');
  }
}
