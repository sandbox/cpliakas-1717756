<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport.
 */

namespace Drupal\ApachesolrStats\Backend\Database\Report;

use Drupal\ApachesolrStats\Report\StatsReport as StatsReport;
use Drupal\ApachesolrStats\Backend\StatsBackend as StatsBackend;

/**
 * Returns report data for keywords.
 */
class TopKeywordsNoClickThroughsReport extends TopKeywordsBase {

  /**
   * Implements Drupal\ApachesolrStats\Report\StatsReport::getReportData().
   */
  public function getReportData() {
    $options = $this->getOptions();

    // Get inner query, add join and condition to check click-throughs.
    $inner_query = $this->getInnerQuery($options);
    $inner_query->leftJoin(StatsBackend::EVENT_LOG, 'asel', 'asql.query_id = asel.query_id');
    $inner_query->isNull('asel.lid');

    // Build the final query, render report.
    $query = $this->getOuterQuery($inner_query, $options);
    return $this->backend->queryToArray($query, 'keywords');
  }
}
