<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport.
 */

namespace Drupal\ApachesolrStats\Backend\Database\Report;

use Drupal\ApachesolrStats\Report\StatsReport as StatsReport;
use Drupal\ApachesolrStats\Backend\StatsBackend as StatsBackend;

/**
 * Returns report data for click-throughs based on keyword.
 */
class KeywordClickThroughsReport extends StatsReport {

  /**
   * Implements Drupal\ApachesolrStats\Report\StatsReport::getReportData().
   */
  public function getReportData() {
    $options = $this->options + array(
      'direction' => 'DESC',
      'keyowrds' => '',
      'limit' => StatsReport::DEFAULT_LIMIT,
      'start_time' => 0,
      'end_time' => 0,
    );

    $inner = db_select(StatsBackend::QUERY_LOG, 'asql', array('target' => 'slave'))
      ->fields('asql', array('query_id'))
      ->condition('asql.keywords', $options['keywords'])
      ->groupBy('query_id');

    $query = db_select($inner, 'asql_inner', array('target' => 'slave'));
    $query->fields('asel', array('document_id', 'url'));
    $query->addExpression('COUNT(asel.document_id)', 'num');
    $query->innerJoin(StatsBackend::EVENT_LOG, 'asel', 'asql_inner.query_id = asel.query_id');
    $query->groupBy('document_id');
    $query->orderBy('num', $options['direction']);
    $query->orderBy('document_id');

    if ($options['start_time']) {
      $query->condition('asel.timestamp', $options['start_time'], '>=');
    }
    if ($options['end_time']) {
      $query->condition('asel.timestamp', $options['end_time'], '<=');
    }
    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }

    return $this->backend->queryToArray($query, 'url');
  }
}
