<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\Database\Report\TopKeywordsReport.
 */

namespace Drupal\ApachesolrStats\Backend\Database\Report;

/**
 * Returns report data for keywords.
 */
class TopKeywordsReport extends TopKeywordsBase {

  /**
   * Implements Drupal\ApachesolrStats\Report\StatsReport::getReportData().
   */
  public function getReportData() {
    $options = $this->getOptions();
    $inner_query = $this->getInnerQuery($options);
    $query = $this->getOuterQuery($inner_query, $options);
    return $this->backend->queryToArray($query, 'keywords');
  }
}
