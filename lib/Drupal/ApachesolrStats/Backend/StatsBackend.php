<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\Backend\StatsBackend.
 */

namespace Drupal\ApachesolrStats\Backend;

/**
 * Abstract class implemented by all statistics backend adapters.
 */
abstract class StatsBackend {

  const QUERY_LOG = 'apachesolr_stats_query_log';
  const EVENT_LOG = 'apachesolr_stats_event_log';

  /**
   * Writes the query log data to the backend.
   *
   * @param array $data
   *   An associative array of data being written to the query log.
   * @param string $bin
   *   The name of the log bin. See class constants.
   * @param array $options
   *   An optional array of additional options to pass to the backend.
   */
  abstract public function write(array $data, $bin, array $options = array());

  /**
   * Performs any cleanup operations.
   *
   * @param string $bin
   *   The name of the log bin. See class constants.
   * @param array $options
   *   An optional array of additional options to pass to the backend.
   */
  abstract public function purge($bin, array $options = array());
}
