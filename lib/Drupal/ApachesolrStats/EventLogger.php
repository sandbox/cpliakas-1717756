<?php

/**
 * @file
 * Contains Drupal\ApachesolrStats\EventLogger.
 */

namespace Drupal\ApachesolrStats;

use Drupal\ApachesolrStats\Backend\StatsBackend as StatsBackend;

/**
 * Class that logs events that happen after a user executes a search.
 */
class EventLogger {

  /**
   * The response object.
   *
   * @var Drupal\ApachesolrStats\EventLogger\HttpResponse
   */
  protected $response;

  /**
   * The variables in the apachesolr_stats table.
   *
   * @var array
   */
  protected $variables;

  /**
   * The backend adapter.
   *
   * @var Drupal\ApachesolrStats\Backend\StatsBackend
   */
  protected $backend;

  /**
   * The shared secret key used for generating the HMAC variant of the message
   * digest.
   *
   * @var string
   */
  protected $key;

  /**
   * Constructs a Drupal\ApachesolrStats\Report\StatsReport object.
   *
   * @param array $variables
   *   An array of variables returned by the SQL query executed in the
   *   bootstrapper.
   */
  public function __construct(array $variables) {
    $this->response = new EventLogger\HttpResponse();
    $this->variables = $variables;

    try {
      // Ensures required variables are loaded.
      if (!$class = $this->getVariable('backend_class')) {
        throw new Exception('The "backend_class" variable is required.');
      }
      if (!$this->key = $this->getVariable('key')) {
        throw new Exception('The "key" variable is required.');
      }

      // Instantiates the backend adapter.
      $this->backend = new $class();
    }
    catch (Exception $e) {
      // @todo Can we log this in a generic way?
      $this->response->error();
    }
  }

  /**
   * Returns a variable.
   *
   * @param string $name
   *   The name of the variable.
   * @param mixed $default
   *   The default if the variable is not defined, defaults to NULL.
   *
   * @return mixed
   *   The variable's value.
   */
  public function getVariable($name, $default = NULL) {
    return isset($this->variables[$name]) ? $this->variables[$name][0] : $default;
  }

  /**
   * Method called by the bootstrapper to run the event logger.
   */
  public function run() {

    // Ensure a hash was passed.
    if (empty($_GET['hash'])) {
      $this->response->badRequest();
    }

    // Get the params from the query string, validate the hash.
    $params = apachesolr_stats_get_hash_params($_GET, TRUE);
    if ($_GET['hash'] != apachesolr_stats_hmac($params, $this->key)) {
      $this->response->badRequest();
    }

    // Registers shutdown function and send response.
    register_shutdown_function(array($this, 'log'), 'view');
    if (empty($_GET['no_redirect'])) {
      $this->response->redirect($_GET['url']);
    }
    else {
      $this->response->ok();
    }
  }

  /**
   * Logs the data passed in the URL.
   *
   * @param string $action
   *   The action being performed, such as "view".
   */
  public function log($action) {
    $data = array(
      'action' => $action,
      'query_id' => $_GET['query_id'],
      'timestamp' => REQUEST_TIME,
      'document_id' => $_GET['document_id'],
      'url' => $_GET['url'],
    );

    try {
      $this->backend->write($data, StatsBackend::EVENT_LOG);
    }
    catch (Exception $e) {
      // @todo Can we log this in a generic way?
    }
  }
}
